package main

import (
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/git"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
)

// analyze runs the tools and produces a report containing issues for each detected secret leak.
func analyze(c *cli.Context, path string, startTime issue.ScanTime) (*issue.Report, error) {
	var err error

	// Load custom config if available
	rulesetPath := filepath.Join(path, ruleset.PathSecretDetection)
	customRuleset, err := ruleset.Load(rulesetPath, "secrets")
	if err != nil {
		switch err.(type) {
		case *os.PathError:
			// Couldn't load an optional custom ruleset file
			log.Debug(err)
		default:
			// TODO: replace with type check
			// The ruleset file did not include a `secrets` analyzer section.
			if strings.HasPrefix(err.Error(), "unable to find a custom ruleset") {
				log.Debug(err)
			} else {
				return nil, err
			}
		}
	}

	pathGitleaksConfig, err := gitleaks.ConfigPath(path, customRuleset)
	if err != nil {
		return nil, err
	}

	// First, check if this scan is a historic/commit range scan or current scan
	if !gitleaks.IsHistoric(c) {
		log.Info("Running non-historic scan")
		// Place the files in a new git repository, in one single commit.
		// This is needed as the tools search the commit history for secret leaks and
		// searching into the full history would find leaks that don't exist anymore.
		path, err = git.FlattenRepo(path)
		if err != nil {
			log.Errorf("Couldn't prepare the repository for analysis: %v\n", err)
			return nil, err
		}
	}

	// Run the tools.
	issues, err := gitleaks.Run(c, path, pathGitleaksConfig)
	if err != nil {
		log.Errorf("Gitleaks analysis failed: %v\n", err)
		return nil, err
	}

	// Deduplicate Gitleaks issues, remove entropy issues contained in others and consolidate
	// them into a single issue if present on several consecutive lines
	issues = consolidateEntropyIssues(cleanEntropyIssues(issues))

	sort.Sort(ByName(issues))

	// Return the report
	if issues == nil {
		// We need to initialize the slice for correct JSON marshalling
		issues = []issue.Issue{}
	}
	report := issue.NewReport()
	report.Vulnerabilities = issues
	report.Scan.Scanner = metadata.ReportScanner
	report.Scan.Type = metadata.Type
	report.Scan.Status = issue.StatusSuccess
	report.Scan.StartTime = &startTime
	endTime := issue.ScanTime(time.Now())
	report.Scan.EndTime = &endTime

	return &report, nil
}
