package main

import (
	"reflect"

	"github.com/mitchellh/copystructure"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// deduplicate deduplicates issues based on their Name, File, and LineStart fields. It also filter out files as per
// the argument.
func deduplicate(issues []issue.Issue) []issue.Issue {

	type dedupKey struct {
		Name      string
		File      string
		Line      int
		CommitSha string
	}

	// At this point LineEnd is equal to LineStart in Gitleaks issues, so it is not taken into account.
	encountered := map[dedupKey]bool{}
	var result []issue.Issue
	for _, i := range issues {
		if !encountered[dedupKey{i.Name, i.Location.File, i.Location.LineStart, i.Location.Commit.Sha}] {
			encountered[dedupKey{i.Name, i.Location.File, i.Location.LineStart, i.Location.Commit.Sha}] = true
			result = append(result, i)
		}
	}
	return result
}

// cleanEntropyIssues removes entropy issues that are included in other issues; typically into
// multiline RSA key issues.
func cleanEntropyIssues(issues []issue.Issue) []issue.Issue {
	var result []issue.Issue
Outer:
	for _, i := range issues {
		if i.Identifiers[0].Value == "Entropy" {
			for _, i2 := range issues {
				if reflect.DeepEqual(i, i2) {
					continue
				}
				if i.Location.File == i2.Location.File && i.Location.LineStart >= i2.Location.LineStart && i.Location.LineEnd <= i2.Location.LineEnd {
					// i is included in i2, skip it.
					continue Outer
				}
			}
		}
		result = append(result, i)
	}
	return result
}

// consolidateEntropyIssues consolidates entropy issues that span several lines.
func consolidateEntropyIssues(issues []issue.Issue) []issue.Issue {
	lastEntropyIssue := issue.Issue{}
	var result []issue.Issue
	for _, i := range issues {
		if i.Identifiers[0].Value == "Entropy" {
			if lastEntropyIssue.Name != "" {
				if i.Location.File == lastEntropyIssue.Location.File && i.Location.LineStart == lastEntropyIssue.Location.LineEnd+1 {
					// This line may be a continuation of the previous line, consolidate.
					lastEntropyIssue.Location.LineEnd = i.Location.LineStart
					lastEntropyIssue.Name = "High entropy block"
					lastEntropyIssue.Description = "Several lines with high entropy were found, this could be a secret."
				} else {
					// This is an new Entropy secret unrelated to the previous one, append the secret contained in
					// lastEntropyIssue.
					result = append(result, lastEntropyIssue)
					// And start consolidating this new entropy secret.
					dup, err := copystructure.Copy(i)
					if err != nil {
						// This should never happen
						panic(err)
					}
					lastEntropyIssue = dup.(issue.Issue)
				}
			} else {
				// The first entropy secret has been found, start consolidating it.
				dup, err := copystructure.Copy(i)
				if err != nil {
					// This should never happen
					panic(err)
				}
				lastEntropyIssue = dup.(issue.Issue)
			}
		} else {
			// This line isn't an Entropy secret, append the secret contained in lastEntropyIssue if it exists
			if lastEntropyIssue.Location.LineStart > 0 {
				result = append(result, lastEntropyIssue)
				lastEntropyIssue = issue.Issue{}
			}
			result = append(result, i)
		}
	}
	if lastEntropyIssue.Location.LineStart > 0 {
		// Append the secret contained in lastEntropyIssue, it was the last secret.
		result = append(result, lastEntropyIssue)
	}
	return result
}
