module gitlab.com/gitlab-org/security-products/analyzers/secrets/v3

require (
	github.com/google/go-cmp v0.5.2
	github.com/mitchellh/copystructure v1.0.0
	github.com/otiai10/copy v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.20.3
	gopkg.in/src-d/go-git.v4 v4.13.1
)

go 1.15
