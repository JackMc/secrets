package gitleaks

import (
	"io/ioutil"
	"path/filepath"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/ruleset"
)

func TestConfigPath(t *testing.T) {
	rootPath := "/root/path"
	tests := []struct {
		name string
		in   *ruleset.Ruleset
		want string
	}{
		{
			"nil ruleset",
			nil,
			DefaultPathGitleaksConfig,
		},
		{
			"passthrough without a mathing target",
			&ruleset.Ruleset{
				PassThrough: []ruleset.PassThrough{
					{
						Type:  ruleset.PassThroughFile,
						Value: "gitleaks-config.toml",
					},
				},
			},
			DefaultPathGitleaksConfig,
		},
		{
			"PassThrough File",
			&ruleset.Ruleset{
				PassThrough: []ruleset.PassThrough{
					{
						Type:   ruleset.PassThroughFile,
						Target: "gitleaks.toml",
						Value:  "gitleaks-config.toml",
					},
				},
			},
			filepath.Join(rootPath, "gitleaks-config.toml"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path, err := ConfigPath(rootPath, tt.in)

			if err != nil {
				t.Errorf("Expected a nil err, but got: %v", err)
			}

			if path != tt.want {
				t.Errorf("got %s, want %s", path, tt.want)
			}
		})
	}
}

func TestConfigPathWithPassThroughRaw(t *testing.T) {
	rawGitleaksConfig := "the most raw"
	rs := &ruleset.Ruleset{
		PassThrough: []ruleset.PassThrough{
			{
				Type:   ruleset.PassThroughRaw,
				Target: "gitleaks.toml",
				Value:  rawGitleaksConfig,
			},
		},
	}
	path, err := ConfigPath("/root/path", rs)

	if err != nil {
		t.Errorf("Expected a nil err, but got: %v", err)
	}

	content, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("tried to read %s, but got %v", path, err)
	}

	contentString := string(content)
	if contentString != rawGitleaksConfig {
		t.Errorf("got %s, want %s", content, rawGitleaksConfig)
	}
}
