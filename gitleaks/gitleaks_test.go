package gitleaks

import (
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestToIssues(t *testing.T) {
	in := `[
		{
			"line": "-----BEGIN RSA PRIVATE KEY-----",
			"lineNumber": 1,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"rule": "RSA",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/id_rsa",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const AWS_ID = \"AKIAIOSFODNN7EXAMPLE\"",
			"lineNumber": 7,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "AKIAIOSFODNN7EXAMPLE",
			"rule": "AWS",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const WORDPRESS = \"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9 HZ\"",
			"lineNumber": 9,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "\"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9",
			"rule": "Entropy: 5.30",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const SSHKEY = ` + "`" + `-----BEGIN RSA PRIVATE KEY-----",
			"lineNumber": 11,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"rule": "RSA",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "const STRIPE = \"sk_test_4eC39HqLyjWDarjtT1zdp7dc\"",
			"lineNumber": 50,
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "sk_test_4eC39HqLyjWDarjtT1zdp7dc",
			"rule": "Stripe",
			"commitMessage": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const API_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"lineNumber": 52,
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "API_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"rule": "Generic API Key",
			"commitMessage": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const APIKEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"lineNumber": 53,
			"commit": "6b997965bc2146f957c9d736eb4918ac644847b8",
			"offender": "APIKEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"rule": "Generic API Key",
			"commitMessage": "Add test api keys ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T12:51:19-04:00"
		},
		{
			"line": "const APITOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"lineNumber": 54,
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "APITOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"rule": "Generic API Key",
			"commitMessage": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "const API_TOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"lineNumber": 55,
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "API_TOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
			"rule": "Generic API Key",
			"commitMessage": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "const HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"",
			"lineNumber": 10,
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"",
			"rule": "Heroku API Key",
			"commitMessage": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "RUN pip install --extra-index-url https://exposed_username:exposed_password@$JFROG_URL/path/to/repo -r requirements.txt",
			"lineNumber": 9,
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "https://exposed_username:exposed_password@$JFROG_URL/path/to/repo",
			"rule": "Password in URL",
			"commitMessage": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/Dockerfile",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "const SLACK_API_TOKEN = \"xoxp-905439787527-905447633015-963063100946-525994fbdbee1ef3096438e693ca6cf2\"",
			"lineNumber": 65,
			"commit": "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "SLACK_API_TOKEN = \"xoxp-905439787527-905447633015-963063100946-525994fbdbee1ef3096438e693ca6cf2\"",
			"rule": "Slack Token",
			"commitMessage": "Replace broken php file by a go one ",
			"author": "Philippe Lafoucrière \u003cplafoucriere@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2019-03-23T17:18:16-04:00"
		},
		{
			"line": "this is a custom rule test",
			"lineNumber": 605,
			"commit": "a4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "custom rule test",
			"rule": "Custom Rule",
			"commitMessage": "Why not be vulnerable to custom rules?",
			"author": "Daniel Searles \u003cdsearles@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2020-10-12T17:18:16-04:00"
		},
		{
			"line": "const social_security = \"344-23-7820\"",
			"lineNumber": 67,
			"commit": "a4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
			"offender": "344-23-7820",
			"rule": "Social Security Number",
			"commitMessage": "A social sec number",
			"author": "Daniel Searles \u003cdsearles@gitlab.com\u003e",
			"file": "testdata/main.go",
			"repo": ".",
			"date": "2020-10-12T17:18:16-04:00"
		}
	]`
	want := []issue.Issue{
		{
			Category:    issue.CategorySecretDetection,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/id_rsa:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/id_rsa",
				LineStart: 1,
				LineEnd:   1,
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
			RawSourceCodeExtract: "-----BEGIN RSA PRIVATE KEY-----",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "AWS API key",
			Message:     "AWS API key",
			Description: "Amazon Web Services API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:8a22accb113d641b78b389ccce92fca96acbe2fd4f1701ead6783768fdbe9d8a:AWS",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 7,
				LineEnd:   7,
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID AWS",
					Value: "AWS",
				},
			},
			RawSourceCodeExtract: "AKIAIOSFODNN7EXAMPLE",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "High entropy string",
			Message:     "High entropy string",
			Description: "A string with high entropy was found, this could be a secret",
			CompareKey:  "testdata/main.go:4bc941a5c41c5460ab3d1895453e66d7032ae11dd401154d396f906d4dda0add:Entropy",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 9,
				LineEnd:   9,
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Entropy",
					Value: "Entropy",
				},
			},
			RawSourceCodeExtract: "\"XahV8uKN^/MvXC`4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:430e004686f8f9c2d11ce84da58bd94d1fceb70b0296e46dd8e1ca059ebf7e92:RSA",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 11,
				LineEnd:   11,
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
			RawSourceCodeExtract: "-----BEGIN RSA PRIVATE KEY-----",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Stripe",
			Message:     "Stripe",
			Description: "Stripe API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:0b16c77410a5918254c1b2bdc7576b87d9ffc7dcedf29b7bdca0b423f6209009:Stripe",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 50,
				LineEnd:   50,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T12:51:19-04:00",
					Message: "Add test api keys ",
					Sha:     "6b997965bc2146f957c9d736eb4918ac644847b8",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Stripe",
					Value: "Stripe",
				},
			},
			RawSourceCodeExtract: "sk_test_4eC39HqLyjWDarjtT1zdp7dc",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:9ac057c201cda016677f2ddeb03d4c59991007a13b9e07917d1f8782e9564970:Generic API Key",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 52,
				LineEnd:   52,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T12:51:19-04:00",
					Message: "Add test api keys ",
					Sha:     "6b997965bc2146f957c9d736eb4918ac644847b8",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				},
			},
			RawSourceCodeExtract: "API_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:f4ac6d9fd61b258a20a5d9c5aea0e0b48abc60d50edd149021d875066e6c592b:Generic API Key",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 53,
				LineEnd:   53,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T12:51:19-04:00",
					Message: "Add test api keys ",
					Sha:     "6b997965bc2146f957c9d736eb4918ac644847b8",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				},
			},
			RawSourceCodeExtract: "APIKEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:72c5f23ed973b0cc312a2c05cae2798ec924f82e3154c86af53e9db0101ec22c:Generic API Key",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 54,
				LineEnd:   54,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T17:18:16-04:00",
					Message: "Replace broken php file by a go one ",
					Sha:     "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				},
			},
			RawSourceCodeExtract: "APITOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Generic API Key",
			Message:     "Generic API Key",
			Description: "Unknown API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:ecd55deb344afa69c181ae430c0352de88dd565ab7ed454535824d62f18882fd:Generic API Key",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 55,
				LineEnd:   55,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T17:18:16-04:00",
					Message: "Replace broken php file by a go one ",
					Sha:     "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Generic API Key",
					Value: "Generic API Key",
				},
			},
			RawSourceCodeExtract: "API_TOKEN = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Heroku API key",
			Message:     "Heroku API key",
			Description: "Heroku API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:4c1de1653442f38a11708d1f89afb5fecd7d32bf9e8c19a82c8feb43274294fb:Heroku API Key",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 10,
				LineEnd:   10,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T17:18:16-04:00",
					Message: "Replace broken php file by a go one ",
					Sha:     "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Heroku API Key",
					Value: "Heroku API Key",
				},
			},
			RawSourceCodeExtract: "HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Password in URL",
			Message:     "Password in URL",
			Description: "Password in URL detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/Dockerfile:f72de753a30b8486abd4a995c98daf7110759d800ea5b86dceb119cb6a083c06:Password in URL",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{File: "testdata/Dockerfile",
				LineStart: 9,
				LineEnd:   9,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T17:18:16-04:00",
					Message: "Replace broken php file by a go one ",
					Sha:     "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Password in URL",
					Value: "Password in URL",
				},
			},
			RawSourceCodeExtract: "https://exposed_username:exposed_password@$JFROG_URL/path/to/repo",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "Slack Token",
			Message:     "Slack Token",
			Description: "Slack token detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:a36b1d2a6673cc8d59065240ffc74bc3389d073cd589eca30ebd2c99cfecf4b3:Slack Token",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 65,
				LineEnd:   65,
				Commit: &issue.Commit{
					Author:  "Philippe Lafoucrière <plafoucriere@gitlab.com>",
					Date:    "2019-03-23T17:18:16-04:00",
					Message: "Replace broken php file by a go one ",
					Sha:     "e4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Slack Token",
					Value: "Slack Token",
				},
			},
			RawSourceCodeExtract: "SLACK_API_TOKEN = \"xoxp-905439787527-905447633015-963063100946-525994fbdbee1ef3096438e693ca6cf2\"",
		},
		{
			Category:             issue.CategorySecretDetection,
			Name:                 "Gitleaks rule Custom Rule",
			Message:              "Gitleaks rule Custom Rule",
			Description:          "Gitleaks rule Custom Rule detected a secret",
			CompareKey:           "testdata/main.go:ed0710ea4b9bc189c991020cbc5c1cb6d6428ca23fcdbc3d742c7434aa65d6fd:Custom Rule",
			Severity:             issue.SeverityLevelCritical,
			Confidence:           issue.ConfidenceLevelUnknown,
			Solution:             "",
			RawSourceCodeExtract: "custom rule test",
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{File: "testdata/main.go",
				LineStart: 605,
				LineEnd:   605,
				Commit: &issue.Commit{
					Author:  "Daniel Searles <dsearles@gitlab.com>",
					Date:    "2020-10-12T17:18:16-04:00",
					Message: "Why not be vulnerable to custom rules?",
					Sha:     "a4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID Custom Rule",
					Value: "Custom Rule",
				}},
		},
		{
			Category:             "secret_detection",
			Name:                 "Social Security Number",
			Message:              "Social Security Number",
			Description:          "Social Security Number detected; please remove and revoke it if this is a leak.",
			CompareKey:           "testdata/main.go:9fc7163b4ec9446e76a29b2e1b49196f2081d6b27a9cc34e1ac42a31d61c7b65:Social Security Number",
			Severity:             issue.SeverityLevelCritical,
			Confidence:           issue.ConfidenceLevelUnknown,
			RawSourceCodeExtract: "344-23-7820",
			Scanner:              issue.Scanner{ID: "gitleaks", Name: "Gitleaks"},
			Location: issue.Location{
				File: "testdata/main.go",
				Commit: &issue.Commit{
					Author:  "Daniel Searles <dsearles@gitlab.com>",
					Date:    "2020-10-12T17:18:16-04:00",
					Message: "A social sec number",
					Sha:     "a4f26145aa93c1f1e45a39d4c1bbc2a097d14565",
				},
				LineStart: 67,
				LineEnd:   67,
			},
			Identifiers: []issue.Identifier{
				{
					Type: "gitleaks_rule_id",
					Name: "Gitleaks rule ID Social Security Number",
					Value: "Social Security Number",
				},
			},
		},
	}
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	r := strings.NewReader(in)
	got, err := toIssues(filepath.Join(dir, "..", "test", "fixtures"), r, false, true)
	if err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("toIssues() results mismatch (-want +got):\n%s", diff)
	}
}

func TestHistoricIssues(t *testing.T) {
	in := `[
		{
			"line": "-----BEGIN RSA PRIVATE KEY-----",
			"lineNumber": 1,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"rule": "RSA",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/id_rsa",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		},
		{
			"line": "-----BEGIN RSA PRIVATE KEY-----",
			"lineNumber": 1,
			"commit": "5ec1e27c70c81b5cc060eed22166790bd59286f1",
			"offender": "-----BEGIN RSA PRIVATE KEY-----",
			"rule": "RSA",
			"commitMessage": "i ",
			"author": "Gilbert Roulot \u003cgroulot@gitlab.com\u003e",
			"file": "testdata/DOES_NOT_EXIST.txt",
			"repo": ".",
			"date": "2019-02-15T14:38:12+01:00"
		}
	]
	`
	want := []issue.Issue{
		{
			Category:    issue.CategorySecretDetection,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "Historic RSA secret has been found in commit 5ec1e27c70c81b5cc060eed22166790bd59286f1.",
			CompareKey:  "testdata/id_rsa:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File: "testdata/id_rsa",
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
				LineStart: 1,
				LineEnd:   1,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
			RawSourceCodeExtract: "-----BEGIN RSA PRIVATE KEY-----",
		},
		{
			Category:    issue.CategorySecretDetection,
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "Historic RSA secret has been found in commit 5ec1e27c70c81b5cc060eed22166790bd59286f1.",
			CompareKey:  "testdata/DOES_NOT_EXIST.txt:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA",
			Severity:    issue.SeverityLevelCritical,
			Confidence:  issue.ConfidenceLevelUnknown,
			Scanner: issue.Scanner{
				ID:   "gitleaks",
				Name: "Gitleaks",
			},
			Location: issue.Location{
				File: "testdata/DOES_NOT_EXIST.txt",
				Commit: &issue.Commit{
					Author:  "Gilbert Roulot <groulot@gitlab.com>",
					Date:    "2019-02-15T14:38:12+01:00",
					Message: "i ",
					Sha:     "5ec1e27c70c81b5cc060eed22166790bd59286f1",
				},
				LineStart: 1,
				LineEnd:   1,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "gitleaks_rule_id",
					Name:  "Gitleaks rule ID RSA",
					Value: "RSA",
				},
			},
			RawSourceCodeExtract: "-----BEGIN RSA PRIVATE KEY-----",
		},
	}
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	r := strings.NewReader(in)
	got, err := toIssues(filepath.Join(dir, "..", "test", "fixtures"), r, true, true)
	if err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("toIssues() results mismatch (-want +got):\n%s", diff)
	}
}

func TestLimitCommits(t *testing.T) {
	tests := []struct {
		commits     []string
		wantCommits []string
		wantErr     bool
		gitDepth    string
	}{
		{
			commits:     []string{"1", "2", "3", "4", "5"},
			wantCommits: []string{"1", "2", "3", "4", "5"},
		},
		{
			commits:     []string{"1", "2", "3", "4", "5"},
			wantCommits: []string{"1", "2"},
			gitDepth:    "3",
		},
		{
			commits:     []string{"1"},
			wantCommits: []string{"1"},
			gitDepth:    "3",
		},
		{
			commits:  []string{"1", "2", "3", "4", "5"},
			wantErr:  true,
			gitDepth: "1",
		},
		{
			commits:     []string{"1", "2"},
			wantCommits: []string{"1"},
			gitDepth:    "2",
		},
		{
			commits:     []string{"1", "2", "3", "4", "5"},
			wantCommits: []string{"1", "2", "3", "4", "5"},
			wantErr:     true,
			gitDepth:    "not-an-int",
		},
		{
			commits:     []string{},
			wantCommits: []string{},
			wantErr:     true,
		},
	}

	gitDepth := os.Getenv("GIT_DEPTH")
	defer os.Setenv("GIT_DEPTH", gitDepth)

	for _, test := range tests {
		if test.gitDepth != "" {
			err := os.Setenv("GIT_DEPTH", test.gitDepth)
			if err != nil {
				t.Fatal(err)
			}
		}
		commits, err := limitCommits(test.commits)
		if err != nil && !test.wantErr {
			t.Fatal(err)
		}
		if test.wantErr {
			if err == nil {
				t.Fatal(err)
			}
		} else {
			if !reflect.DeepEqual(commits, test.wantCommits) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", test.wantCommits, commits)
			}
		}
		os.Setenv("GIT_DEPTH", "")
	}
}
