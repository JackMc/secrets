package gitleaks

import (
	"io/ioutil"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/ruleset"
)

const (
	gitleaksPassThroughTarget = "gitleaks.toml"
)

// ConfigPath will look at rulesets to determine the path for the gitleaks.toml
func ConfigPath(projectPath string, customRuleset *ruleset.Ruleset) (string, error) {
	// Set path to default
	pathGitleaksConfig := DefaultPathGitleaksConfig

	if customRuleset == nil {
		return pathGitleaksConfig, nil
	}

	for _, passThrough := range customRuleset.PassThrough {
		if passThrough.Target == gitleaksPassThroughTarget {
			switch passThrough.Type {
			case ruleset.PassThroughFile:
				pathGitleaksConfig = filepath.Join(projectPath, passThrough.Value)
			case ruleset.PassThroughRaw:
				content := []byte(passThrough.Value)
				tmpfile, err := ioutil.TempFile("", "gitleaks.toml")
				if err != nil {
					return "", err
				}

				log.Debugf("Gitleaks config: %s", passThrough.Value)
				if _, err := tmpfile.Write(content); err != nil {
					return "", err
				}

				if err := tmpfile.Close(); err != nil {
					return "", err
				}

				pathGitleaksConfig = tmpfile.Name()
				log.Debugf("Gitleaks config path: %s", pathGitleaksConfig)
			}
		}
	}

	return pathGitleaksConfig, nil
}
