package gitleaks

import (
	"fmt"
)

// Rule contain the name and description for a rule.
type Rule struct {
	Name        string
	Description string
}

// singleCredDesc formats a single credential description
func singleCredDesc(subject string) string {
	return fmt.Sprintf("%s detected; please remove and revoke it if this is a leak.", subject)
}

// See scanner/gitleaks/gitleaks.toml for a list of Gitleaks rules.
var rules = map[string]Rule{
	"AWS": {
		Name:        "AWS API key",
		Description: singleCredDesc("Amazon Web Services API key"),
	},
	"Facebook": {
		Name:        "Facebook token",
		Description: singleCredDesc("Facebook token"),
	},
	"Github": {
		Name:        "GitHub token",
		Description: singleCredDesc("GitHub token"),
	},
	"PGP": {
		Name:        "PGP private key",
		Description: singleCredDesc("PGP private key"),
	},
	"PKCS8": {
		Name:        "PKCS8 key",
		Description: singleCredDesc("PKCS8 private key"),
	},
	"RSA": {
		Name:        "RSA private key",
		Description: singleCredDesc("RSA private key"),
	},
	"Slack Token": {
		Name:        "Slack Token",
		Description: singleCredDesc("Slack token"),
	},
	"Slack Webhook": {
		Name:        "Slack Webhook",
		Description: singleCredDesc("Slack webhook"),
	},
	"Stripe": {
		Name:        "Stripe",
		Description: singleCredDesc("Stripe API key"),
	},
	"SSH": {
		Name:        "SSH private key",
		Description: singleCredDesc("SSH private key"),
	},
	"Twitter": {
		Name:        "Twitter key",
		Description: singleCredDesc("Twitter key"),
	},
	"Generic API Key": {
		Name:        "Generic API Key",
		Description: singleCredDesc("Unknown API key"),
	},
	"Generic Secret plus Entropy": {
		Name:        "Generic Secret plus Entropy",
		Description: singleCredDesc("Unknown Secret"),
	},
	"Google (GCP) Service-account": {
		Name:        "Google GCP service account",
		Description: singleCredDesc("Google GCP service account"),
	},
	"Heroku API Key": {
		Name:        "Heroku API key",
		Description: singleCredDesc("Heroku API key"),
	},
	"Password in URL": {
		Name:        "Password in URL",
		Description: singleCredDesc("Password in URL"),
	},
	"SSH (DSA) private key": {
		Name:        "SSH DSA private key",
		Description: singleCredDesc("SSH DSA private key"),
	},
	"SSH (EC) private key": {
		Name:        "SSH EC private key",
		Description: singleCredDesc("SSH EC private key"),
	},
	"Twilio API Key": {
		Name:        "Twilio API key",
		Description: singleCredDesc("Twilio API key"),
	},
	"Social Security Number": {
		Name:        "Social Security Number",
		Description: singleCredDesc("Social Security Number"),
	},
}
