package gitleaks

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/utils"

	"github.com/urfave/cli"
)

const (
	// FlagEntropyLevel is a gitleaks entropy level 0-8
	FlagEntropyLevel = "gitleaks-entropy-level"
	// CommitFrom is the commit a Gitleaks scan starts at.
	CommitFrom = "commit-from"
	// CommitTo is the commit a Gitleaks scan ends at.
	CommitTo = "commit-to"
	// Commits is a list of comma separated commits for Gitleaks to scan
	Commits = "commits"
	// CommitsFile is a file containing a list of commits for Gitleaks to scan
	CommitsFile = "commits-file"
	// FlagHistoricScan is flag to enable a historic scan.
	FlagHistoricScan = "full-scan"
	// DefaultEntropy is 8.0
	DefaultEntropy = 8.0

	// DefaultPathGitleaksConfig is the path to the gitleaks config.
	DefaultPathGitleaksConfig = "/gitleaks.toml"

	envVarCommitsFile = "SECRET_DETECTION_COMMITS_FILE"
	envVarCommits     = "SECRET_DETECTION_COMMITS"
	envVarEntropy     = "SECRET_DETECTION_ENTROPY_LEVEL"
	envVarCommitFrom  = "SECRET_DETECTION_COMMIT_FROM"
	envVarCommitTo    = "SECRET_DETECTION_COMMIT_TO"
	envVarFullScan    = "SECRET_DETECTION_HISTORIC_SCAN"
	pathGitleaks      = "gitleaks"
	entropyRuleTmpl   = `
[[rules]]
description = "Generic Secret plus Entropy"
regex = '''(?i)(api_key|apikey|secret|key|api|password|pw)'''
entropies = ["%f-8.0"]
`
)

// Secret represents a gitleaks leak
type Secret struct {
	Line       string `json:"line"`
	LineNumber int    `json:"lineNumber"`
	Offender   string `json:"offender"`
	Rule       string `json:"rule"`
	Commit     string `json:"commit"`
	File       string `json:"file"`
	Message    string `json:"commitMessage"`
	Author     string `json:"author"`
	Date       string `json:"date"`
}

// MakeFlags returns the cli flags.
func MakeFlags() []cli.Flag {
	return []cli.Flag{
		cli.Float64Flag{
			Name:   FlagEntropyLevel,
			Usage:  "Gitleaks entropy level (0.0 to 8.0)",
			EnvVar: envVarEntropy,
			Value:  DefaultEntropy,
		},
		cli.StringFlag{
			Name:   CommitFrom,
			Usage:  "Run a scan on a range of commits starting at this commit",
			EnvVar: envVarCommitFrom,
		},
		cli.StringFlag{
			Name:   CommitTo,
			Usage:  "Run a scan on a range of commits stopping at this commit",
			EnvVar: envVarCommitTo,
		},
		cli.BoolFlag{
			Name:   FlagHistoricScan,
			Usage:  "Runs an historic (all commits) scan on the repository",
			EnvVar: envVarFullScan,
		},
		cli.StringFlag{
			Name:   Commits,
			Usage:  "Commits is a list of comma separated commits for Gitleaks to scan",
			EnvVar: envVarCommits,
		},
		cli.StringFlag{
			Name:   CommitsFile,
			Usage:  "CommitsFile is a file containing a list of commits delimited by newlines for Gitleaks to scan",
			EnvVar: envVarCommitsFile,
		},
	}
}

// Run runs Gitleaks on the path and transforms results into issues.
func Run(c *cli.Context, path, pathGitleaksConfig string) ([]issue.Issue, error) {
	var historic bool

	isDefaultGitleaksConfig := pathGitleaksConfig == DefaultPathGitleaksConfig

	// Create a temporary file. Gitleaks can't output to stdout.
	tmpFile, err := ioutil.TempFile("", "gitleaks-*.json")
	if err != nil {
		log.Errorf("Couldn't create temporary file: %v\n", err)
		return nil, err
	}
	defer utils.WithWarning(fmt.Sprintf("couldn't close temporary file %s", tmpFile.Name()), tmpFile.Close)

	if c.Float64(FlagEntropyLevel) != DefaultEntropy {
		if err := addEntropyRule(c.Float64(FlagEntropyLevel), pathGitleaksConfig); err != nil {
			return nil, err
		}
	}

	// This is the default command for gitleaks
	cmd := exec.Command(pathGitleaks, "--report", tmpFile.Name(), "--repo-path", path, "--config", pathGitleaksConfig)

	if c.Bool(FlagHistoricScan) {
		historic = true
	} else if c.String(Commits) != "" {
		historic = true
		commits, err := limitCommits(strings.Split(c.String(Commits), ","))
		if err != nil {
			return []issue.Issue{}, err
		}
		log.Infof("Running scan on commits %s..%s", commits[0], commits[len(commits)-1])
		cmd = exec.Command(pathGitleaks, "--report", tmpFile.Name(), "--repo-path", path,
			"--config", pathGitleaksConfig, "--commits", strings.Join(commits, ","))
	} else if c.String(CommitsFile) != "" {
		historic = true
		content, err := ioutil.ReadFile(c.String(CommitsFile))
		if err != nil {
			return []issue.Issue{}, err
		}
		commits, err := limitCommits(strings.Split(string(content), "\n"))
		if err != nil {
			return []issue.Issue{}, err
		}
		log.Infof("Running scan from commits file %s on commits %s..%s", c.String(CommitsFile), commits[0], commits[len(commits)-1])
		log.Debug(commits)
		cmd = exec.Command(pathGitleaks, "--report", tmpFile.Name(), "--repo-path", path,
			"--config", pathGitleaksConfig, "--commits", strings.Join(commits, ","))
	} else if c.String(CommitTo) != "" && c.String(CommitFrom) != "" {
		log.Infof("Running scan from commits %s to %s", c.String(CommitFrom), c.String(CommitTo))
		historic = true
		cmd = exec.Command(pathGitleaks, "--report", tmpFile.Name(), "--repo-path", path,
			"--config", pathGitleaksConfig, "--commit-to", c.String(CommitTo), "--commit-from", c.String(CommitFrom))
	}

	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	// Gitleaks exits with these status codes:
	// 0: no leaks
	// 1: leaks present
	// 2: error encountered
	if err == nil {
		return []issue.Issue{}, nil // no leaks
	}
	if exitErr, ok := err.(*exec.ExitError); ok {
		if exitErr.Sys().(syscall.WaitStatus).ExitStatus() == 1 {
			return toIssues(path, tmpFile, historic, isDefaultGitleaksConfig)
		}
	}
	log.Errorf("Couldn't run the gitleaks command: %v\n", err)
	return nil, err
}

// limitCommits will check if GET_DEPTH is set and ignore commits that exceed GIT_DEPTH
func limitCommits(commits []string) ([]string, error) {
	if len(commits) == 0 {
		return commits, fmt.Errorf("no commits to scan")
	}
	if os.Getenv("GIT_DEPTH") != "" {
		gitDepth, err := strconv.Atoi(os.Getenv("GIT_DEPTH"))
		if err != nil {
			return commits, err
		}
		if gitDepth == 1 {
			return commits, fmt.Errorf("unable to run secret-detection job with a GIT_DEPTH < 2")
		}
		if gitDepth > len(commits) {
			return commits, nil
		}
		// We return commits[:gitDepth-1] because gitleaks needs a parent commit for each commit scanned.
		// See comment: https://gitlab.com/gitlab-org/gitlab/-/issues/247261#note_410868888
		return commits[:gitDepth-1], nil
	}
	return commits, nil
}

func addEntropyRule(entropy float64, pathGitleaksConfig string) error {
	// append regex rule to the gitleaks config to maintain old functionality
	rule := fmt.Sprintf(entropyRuleTmpl, entropy)
	f, err := os.OpenFile(pathGitleaksConfig, os.O_APPEND|os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.WriteString(rule); err != nil {
		return err
	}
	return nil
}

// toIssues converts a Gitleaks report into issues while adding line number.
func toIssues(path string, reportFile io.Reader, historic, isDefaultGitleaksConfig bool) ([]issue.Issue, error) {
	var secrets []Secret

	// Decode JSON report
	err := json.NewDecoder(reportFile).Decode(&secrets)
	if err != nil {
		log.Errorf("Couldn't parse the Gitleaks report: %v\n", err)
		return nil, err
	}

	// Translate to issues
	var issues []issue.Issue
	for _, secret := range secrets {
		sourceCode := secret.Line
		filePath := secret.File

		// Compute Name, Description and RuleID.
		// TODO: extract function
		var name, description, ruleID string
		rule, ruleFound := rules[secret.Rule]
		switch {
		case ruleFound:
			name = rule.Name
			description = rule.Description
			ruleID = secret.Rule
		case strings.HasPrefix(secret.Rule, "Entropy: "):
			// A string with high entropy has been detected.
			name = "High entropy string"
			description = "A string with high entropy was found, this could be a secret"
			ruleID = "Entropy"
		default:
			if isDefaultGitleaksConfig {
				// This is an unknown rule in the bundled gitleaks.toml. Warn the user and use default values.
				log.Errorf(
					"No description for Gitleaks rule %s, please open an issue on https://gitlab.com/gitlab-org/gitlab-ee/issues\n",
					secret.Rule)
			}

			name = fmt.Sprint("Gitleaks rule ", secret.Rule)
			description = fmt.Sprint("Gitleaks rule ", secret.Rule, " detected a secret")
			ruleID = secret.Rule
		}

		// TODO remove this once "commit" has been added to the rails vulnerability module
		// See https://gitlab.com/gitlab-org/gitlab/issues/13393
		if historic {
			description = fmt.Sprintf("Historic %s secret has been found in commit %s.", secret.Rule, secret.Commit)
		}

		// create commit object for issue location
		commit := issue.Commit{
			Author:  secret.Author,
			Date:    secret.Date,
			Message: secret.Message,
			Sha:     secret.Commit,
		}

		// append the issue.
		issues = append(issues, issue.Issue{
			Category:             metadata.Type,
			Scanner:              metadata.IssueScanner,
			Name:                 name,
			Message:              name,
			Description:          description,
			CompareKey:           convert.CompareKey(filePath, convert.Fingerprint(sourceCode), ruleID),
			Severity:             issue.SeverityLevelCritical,
			Confidence:           issue.ConfidenceLevelUnknown,
			Location:             convert.Location(filePath, secret.LineNumber, secret.LineNumber, commit),
			Identifiers:          convert.Identifiers("Gitleaks", ruleID),
			RawSourceCodeExtract: secret.Offender,
		})
	}

	return issues, nil
}

// IsHistoric checks if this scan is a historic scan based on ci vars.
func IsHistoric(c *cli.Context) bool {
	if (c.String(CommitTo) != "" && c.String(CommitFrom) != "") || c.Bool(FlagHistoricScan) ||
		c.String(Commits) != "" || c.String(CommitsFile) != "" {
		return true
	}
	return false
}
