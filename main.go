// The analyzer program finds leaks of secrets like token and cryptographic keys in a directory.
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/utils"
)

const (
	flagTargetDir     = "target-dir"
	flagArtifactDir   = "artifact-dir"
	flagExcludedPaths = "excluded-paths"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Author = metadata.AnalyzerVendor
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []cli.Command{runCommand()}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runCommand() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: command.EnvVarTargetDir + "," + command.EnvVarCIProjectDir,
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: command.EnvVarArtifactDir + "," + command.EnvVarCIProjectDir,
		},
		cli.StringSliceFlag{
			Name:   flagExcludedPaths,
			EnvVar: "SECRET_DETECTION_EXCLUDED_PATHS",
			Usage:  "Comma-separated list of paths (globs supported) to be excluded from the output.",
		},
	}

	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, gitleaks.MakeFlags()...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			startTime := issue.ScanTime(time.Now())

			// no args
			if c.Args().Present() {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errors.New("invalid number of arguments")
			}

			// parse excluded paths
			filter, err := pathfilter.NewFilter(c)
			if err != nil {
				return err
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			report, err := analyze(c, targetDir, startTime)
			if err != nil {
				return err
			}

			// filter paths, sort
			report.ExcludePaths(filter.IsExcluded)
			report.Sort()

			// Writing both SAST and Secret Detection artifacts. Eventually we will remove the SAST artifact once
			// it is confirmed that stand-alone secret detection is working properly.
			if err := writeReport(command.ArtifactNameSecretDetection, report, c); err != nil {
				return err
			}
			if err := writeReport(command.ArtifactNameSAST, report, c); err != nil {
				return err
			}

			return nil
		},
	}
}

func writeReport(artifactType string, report *issue.Report, c *cli.Context) error {
	artifactPath := filepath.Join(c.String(flagArtifactDir), artifactType)
	f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer utils.WithWarning(fmt.Sprintf("couldn't close file %s", artifactPath), f.Close)
	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")
	return enc.Encode(report)
}
