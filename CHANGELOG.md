# Secrets analyzer changelog

## v3.11.0
- Add social security number regex to gitleaks toml (!83)

## v3.10.2
- Bump gitleaks to v6.2.0 (!82)

## v3.10.1
- Add `vulnerability.raw_source_code_extract` containing leaked token (!79)

## v3.10.0
- Add custom rulesets (!80)

## v3.9.3
- Update golang dependencies (!75)

## v3.9.2
- Fix bug when `GIT_DEPTH` exceeds listed commits during non-default-branch scans (!72)

## v3.9.1
- Update Dockerfile and golang dependencies to latest versions (!71)

## v3.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!70)

## v3.8.0
- Bump gitleaks to v6.1.2 (!69)
- Add `SECRET_DETECTION_COMMITS` and `SECRET_DETECTION_COMMITS_FILE` options

## v3.7.2
- Upgrade go to version 1.15 (!68)

## v3.7.1
- Bump secrets module from v2 to v3 (!65)

## v3.7.0
- Replace `SAST_EXCLUDED_PATHS` with `SECRET_DETECTION_EXCLUDED_PATHS` (!64)

## v3.6.0
- Use scanner instead of analyzer in `scan.scanner` object (!62)

## v3.5.0
- Bump gitleaks to v5.0.1 (!60)

## v3.4.1
- Increase possible password length in URL regex (!61)

## v3.4.0
- Add scan object to report (!56)

## v3.3.1
- Fix `slack token` reporting (!57)

## v3.3.0
- Remove Trufflehog dependency from the analyzer (!52)

## v3.2.0
- Switch to the MIT Expat license (!54)

## v3.1.0
- Update logging to be standardized across analyzers (!50)

## v3.0.5
- Fixes `SECRET_DETECTION_HISTORIC_SCAN` bug when scanning non-existent files on the default branch (!48)

## v3.0.4
- Bump gitleaks to v4.3.1 (!49)

## v3.0.3
- Change env var prefix from `SAST_GITLEAKS_` to `SECRET_DETECTION_`

## v3.0.2
- Ignore QA test so we can release v3.x. v3.0.1 did not include a changelog entry so we need to bump once more.

## v3.0.1
- Ignore QA test so we can release v3.x

## v3.0.0
- Add standalone secret detection support (!42)

## v2.8.0
- Bump gitleaks and trufflehog (!38)

## v2.7.0
- Adds the `SAST_EXCLUDED_PATHS` env flag (!37)

## v2.6.0
- Add `id` field to vulnerabilities in JSON report (!36)

## v2.5.0
- Add commit range scanning

## v2.4.0
- Add historic scanning (!27)

## v2.3.0
- Add support for custom CA certs (!30)

## v2.2.3
- Update Gitleaks from 1.24.0 to 3.3.0

## v2.2.2
- Add check for env vars in password-in-url vulnerabilities

## v2.2.1
- Update common to v2.1.6

## v2.2.0
- Remove diffence (https://gitlab.com/gitlab-org/security-products/analyzers/secrets/merge_requests/13)

## v2.1.1
- Fix typos in reported messages

## v2.1.0
- Add support for generic api keys (https://gitlab.com/gitlab-org/gitlab/issues/10594)

## v2.0.5
- Set default severity value to Critical

## v2.0.4
- Fix: Set default Gitleaks entropy level to maximum to suppress false positives

## v2.0.3
- Fix: Update gitleaks file reader to better handle large files, fixes buffer overflow
- Fix: Update incorrect gitleaks rule RKCS8 to PKCS8

## v2.0.2
- Add gitleaks config, exclude svg analysis

## v2.0.1
- Fix gitleaks integration: don't parse output when there are no leaks

## v2.0.0
- Initial release
